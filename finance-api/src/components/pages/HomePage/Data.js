export const homeObjOne ={
    lightBg:false,
    lightText:true,
    lightTextDesc:true,
    topLine:'API Data for everyone',
    headline:'Financial data for every needs',
    description:'Build and Explore our Financial API data, we are the largest finance database in the country ',
    buttonLabel:'Get Started',
    imgStart:'',
    img:'images/professionallook.svg',
    alt:"finance"
};
export const homeObjTwo = {
    lightBg: false,
    lightText: true,
    lightTextDesc: true,
    topLine: 'Real Time Stock Price',
    headline: 'Quaterly & Annually Statement',
    description:'We provide real time stock price, we cover the fundamental data part of the stocks prices via cashflow statement, balance sheet statement and income statement twice a year quarterly and annually.',
    buttonLabel: 'Learn More',
    imgStart: '',
    img: 'images/financedata.svg',
    alt: 'Vault'
  };
  export const homeObjThree = {
    lightBg: true,
    lightText: false,
    lightTextDesc: false,
    topLine: 'Back in the day....',
    headline: 'History of prices were a little different',
    description:
      "Today we can provide historical data that goes up to 40 years back in history. Multiple exchanges, financial statements and many more. Our financial statements is updated in real time, that means every API requested is up to date!",
    buttonLabel: 'Start Now',
    imgStart: 'start',
    img: 'images/backintheday.svg',
    alt: 'Vault'
  };
  export const homeObjFour = {
    lightBg: true,
    lightText: false,
    lightTextDesc: false,
    topLine: 'Stocks',
    headline: 'Twousands of Stocks with tons of data ',
    description:
      'Our API supports over 12000 Stocks across multiple exchanges. Whole U.S. market, we cover MUTUAL FUNDS, ETFs(Exchance Trade Fund), BONDS, PRICE OF METALS and many more.',
    buttonLabel: 'Sign Up Now',
    imgStart: 'start',
    img: 'images/stockprices.svg',
    alt: 'Vault'
  };
  export const homeObjFive = {
    lightBg: false,
    lightText: true,
    lightTextDesc: true,
    topLine: 'Give It a Try!',
    headline: 'API KEY here',
    description:
      'Get your Free API Key Today!',
    buttonLabel: 'Sign Up Now',
    imgStart: 'start',
    img: 'images/pinfo.svg',
    alt: 'Vault'
  };