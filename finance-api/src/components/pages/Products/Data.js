export const homeObjOne = {
    lightBg: true,
    lightText: false,
    lightTextDesc: false,
    topLine: 'VIEW OUR PRODUCTS',
    headline: 'Shop through our catalog of products',
    description:
      'We provide worldwide shipping to all countries. If there are any issues, just issue a refund and we will process your request',
    buttonLabel: 'Shop Now',
    imgStart: '',
    img: 'images/svg-4.svg',
    alt: 'Credit Card'
  };
  
  export const homeObjTwo = {
    lightBg: false,
    lightText: true,
    lightTextDesc: true,
    topLine: '100% SECURE',
    headline: 'Stay protected 24/7 anywhere anytime',
    description:
      'We have you covered no matter where you are located. Over 140 locations worldwide to ensure you have access anytime',
    buttonLabel: 'Learn More',
    imgStart: '',
    img: 'images/svg-5.svg',
    alt: 'Vault'
  };
  
  export const homeObjThree = {
    lightBg: true,
    lightText: false,
    lightTextDesc: false,
    topLine: 'Super Fast',
    headline: 'Fast accurate and developer friendly',
    description:
      "We provide one of the most accurate data available on the market. Our API has over 50 endpoints and we are still growing by adding new endpoint",
    buttonLabel: 'Start Now',
    imgStart: 'start',
    img: 'images/fast.png',
    alt: 'Vault'
  };
  
  export const homeObjFour = {
    lightBg: false,
    lightText: true,
    lightTextDesc: true,
    topLine: 'Core Data',
    headline: 'To power a core set of financial data',
    description:
      'We have the most powerful tools that enable developers to build great financial apps using scalable and modern APIs, same as Real-time and historical pricing are necessary to power your modern application.',
    buttonLabel: 'Sign Up Now',
    imgStart: 'start',
    img: 'images/marketing.gif',
    alt: 'Vault'
  };